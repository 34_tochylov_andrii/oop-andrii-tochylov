package ex06;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * ��������� ������� ���������� ��� ���������� ������� ����������� ����������
 * �������
 * 
 * @author Andrei
 * @see AnnotatedObserver
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}