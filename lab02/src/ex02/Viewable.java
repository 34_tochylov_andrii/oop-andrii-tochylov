package ex02;

/** Creator
* (������ ��������������
* Factory Method)<br>
* ��������� �����,
* "�����������" �������
* @author Andrei
* @version 1.0
* @see Viewable#getView()
*/
public interface Viewable {
	
/** ������ ������, ����������� {@linkplain View} */
	public View getView();

}