package ex02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ex01.Number;

/**
 * ConcreteProduct (������ �������������� Factory Method)<br>
 * ���������� �������, ���������� � ����������� �����������
 * 
 * @author Andrei
 * @version 1.0
 * @see View
 */

public class ViewResult implements View {
	/** ��� �����, ������������ ��� ������������ */
	private static final String FNAME = "Numb.bin";

	/** ���������� ���������� �������� ��� ���������� �� ��������� */
	private static final int DEFAULT_NUM = 10;

	/** ��������� ���������� � ����������� ���������� */
	private ArrayList<Number> numbers = new ArrayList<Number>();

	/**
	 * �������� {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)} �
	 * ���������� {@linkplain ViewResult#DEFAULT_NUM DEFAULT_NUM}
	 */
	public ViewResult() {
		this(DEFAULT_NUM);
	}

	/**
	 * �������������� ��������� {@linkplain ViewResult#numbers}
	 * 
	 * @param n ��������� ���������� ���������
	 */
	public ViewResult(int n) {
		for (int ctr = 0; ctr < n; ctr++) {
			numbers.add(new Number());
		}
	}

	/**
	 * �������� �������� {@linkplain ViewResult#items}
	 * 
	 * @return ������� �������� ������ �� ������ {@linkplain ArrayList}
	 */
	public ArrayList<Number> getNumbers() {
		return numbers;
	}

	/**
	 * ��������� �������� ������� � ��������� ��������� � ���������
	 * {@linkplain ViewResult#items}
	 * 
	 * @param stepX ��� ���������� ���������
	 */
	public void init(int stepX) {
		int val = ((int) (Math.random() * 100));
		for (Number numb : numbers) {
			numb.setNumbers(val, calcBin(val), calcOct(val), calcHex(val));
			val += stepX;
		}
	}

	/** ������������ ��������� ����� val � ����������������� ������� */
	private String calcHex(int val) {
		return Integer.toHexString(val);
	}

	/** ������������ ��������� ����� val � ������������ ������� */
	private String calcOct(int val) {
		return Integer.toOctalString(val);
	}

	/** ������������ ��������� ����� val � �������� ������� */
	private String calcBin(int val) {
		return Integer.toBinaryString(val);
	}

	/**
	 * �������� <b>init(double stepX)</b> �� ��������� ��������� ���������<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewInit() {
		init((int) (Math.random() * 100));
	}

	/**
	 * ���������� ������ {@linkplain View#viewSave()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewSave() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(numbers);
		os.flush();
		os.close();
	}

	/**
	 * ���������� ������ {@linkplain View#viewRestore()}<br>
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void viewRestore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		numbers = (ArrayList<Number>) is.readObject();
		is.close();
	}

	/**
	 * ���������� ������ {@linkplain View#viewHeader()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewHeader() {
		System.out.println("Results:");
	}

	/**
	 * ���������� ������ {@linkplain View#viewBody()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewBody() {
		for (Number number : numbers) {
			System.out.println(+number.getValue() + "\t" + number.getBinary() + "\t" + number.getOctal() + "\t"
					+ number.getHexadecimal());
		}
		System.out.println();
	}

	/**
	 * ���������� ������ {@linkplain View#viewFooter()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewFooter() {
		System.out.println("End.");
	}

	/**
	 * ���������� ������ {@linkplain View#viewShow()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();

		viewFooter();
	}
}