package ex07;

import ex02.View;
import ex04.ConsoleCommand;
import ex05.ExecuteConsoleCommand;

/**
 * ���������� ������� Execute all threads; ������ Command
 * 
 * @author Andrei
 * @version 1.0
 */
public class DrawCommand implements ConsoleCommand {
	/**
	 * ������, ����������� ��������� {@linkplain View}; ����������� ���������
	 * �������� {@linkplain ex01.Number}
	 */
	private View view;

	/**
	 * ���������� ���� {@linkplain ExecuteConsoleCommand#view}
	 * 
	 * @return �������� {@linkplain ExecuteConsoleCommand#view}
	 */
	public View getView() {
		return view;
	}

	/**
	 * ������������� ���� {@linkplain ExecuteConsoleCommand#view}
	 * 
	 * @param view �������� ��� {@linkplain ExecuteConsoleCommand#view}
	 * @return ����� �������� {@linkplain ExecuteConsoleCommand#view}
	 */
	public View setView(View view) {
		return this.view = view;
	}

	/**
	 * �������������� ���� {@linkplain ExecuteConsoleCommand#view}
	 * 
	 * @param view ������, ����������� {@linkplain View}
	 */
	public DrawCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'd';
	}

	@Override
	public String toString() {
		return "'d'raw";
	}

	@Override
	public void execute() {
		new Window((ViewWindow) view);
		System.out.println("draw a graph");
	}
}