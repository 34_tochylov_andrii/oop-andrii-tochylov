package ex07;

import ex02.Viewable;
import ex04.Application;

/**
 * ���������� � ����������� �����������; c������� ���������� ������������ ������
 * main()
 * 
 * @author Andrei
 * @version 7.0
 * @see Main#main
 */
public class Main {
	/**
	 * ����������� ��� ������� ���������; �������� �����
	 * {@linkplain Application#run(Viewable)}
	 * 
	 * @param args ��������� ������� ���������
	 */
	public static void main(String[] args) {
		Application app = Application.getInstance();
		app.run(new ViewableWindow());
		System.exit(0);
	}
}