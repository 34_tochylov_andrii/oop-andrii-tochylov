package ex07;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import ex01.Number;
import ex02.ViewResult;

/**
 * �������� ���� ����������� �������
 * 
 * @author Andrei
 * @version 1.0
 * @see Frame
 */
@SuppressWarnings("serial")
public class Window extends Frame {
	/** ������� ������� �� ���� ���� */
	private static final int BORDER = 20;
	/**
	 * ������, ����������� ��������� {@linkplain ex02.View};<br>
	 * ����������� ��������� �������� {@linkplain ex01.Item2d}
	 */
	private ViewResult view;

	/**
	 * �������������� {@linkplain Window#view};<br>
	 * ������� ���������� ������� �������� ����
	 * 
	 * @param view �������� ��� ���� {@linkplain Window#view}
	 */
	public Window(ViewResult view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
//System.exit(0);
				setVisible(false);
			}
		});
	}

	@Override
	public void paint(Graphics g) {
		Rectangle r = getBounds(), c = new Rectangle();
		r.x += BORDER;
		r.y += 25 + BORDER;
		r.width -= r.x + BORDER;
		r.height -= r.y + BORDER;
		c.x = r.x;
		c.y = r.y + r.height / 2;
		c.width = r.width;
		c.height = r.height / 2;
		g.setColor(Color.LIGHT_GRAY);
		g.setColor(Color.RED);
		g.drawLine(c.x, c.y, c.x + c.width, c.y);
		g.drawLine(c.x, r.y, c.x, r.y + r.height);
		view.viewInit();
		double maxX = 0, maxY = Integer.parseInt(view.getNumbers().get(0).getOctal()), scaleX, scaleY;
		for (Number item : view.getNumbers()) {
			if (item.getValue() > maxX)
				maxX = item.getValue();
			if (Math.abs(Integer.parseInt(item.getOctal())) > maxY)
				maxY = Math.abs(Integer.parseInt(item.getOctal()));
		}
		g.drawString("+" + maxY, r.x, r.y);
		g.drawString("-" + maxY, r.x, r.y + r.height);
		g.drawString("+" + maxX, c.x + c.width - g.getFontMetrics().stringWidth("+" + maxX), c.y);
		scaleX = c.width / maxX;
		scaleY = c.height / maxY;
		g.setColor(Color.BLUE);
		for (Number item : view.getNumbers()) {
			g.drawOval(c.x + (int) (item.getValue() * scaleX) - 5,
					c.y - (int) (Integer.parseInt(item.getOctal()) * scaleY) - 5, 10, 10);
			System.out.println("x= " + (c.x + (int) (item.getValue() * scaleX) - 5) + " y= "
					+ (c.y - (int) (Integer.parseInt(item.getOctal()) * scaleY)));
		}
	}
}