package test02;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import junit.framework.Assert;
import java.io.IOException;
import ex01.Number;
import ex02.ViewResult;

/** ��������� ������������
* ������������� �������.
* @author Andrei
* @version 2.0
*/
public class MainTest {
		/** �������� �������� ���������������� ������ {@linkplain ViewResult} */
	@Test
	public void testCalc() {
		ViewResult view = new ViewResult(3);
		view.init(10);
		Number number = new Number();
		int ctr = 0;
		number.setNumbers(0, "0", "0", "0" );
		assertTrue("expected:<" + number + "> but was:<" + view.getNumbers().get(ctr) + ">",
		view.getNumbers().get(ctr).equals(number));
		ctr++;
		System.out.println(ctr);
		number.setNumbers(10, "1010", "12", "a");
		assertTrue("expected:<" + number + "> but was:<" + view.getNumbers().get(ctr) + ">",
		view.getNumbers().get(ctr).equals(number));
		ctr++;
		number.setNumbers(20, "10100", "24","14");
		assertTrue("expected:<" + number + "> but was:<" + view.getNumbers().get(ctr) + ">",
		view.getNumbers().get(ctr).equals(number));
		ctr++;
	}
	/** �������� ������������. ������������ �������������� ������. */
	@Test
	public void testRestore() {
		ViewResult view1 = new ViewResult(1000);
		ViewResult view2 = new ViewResult();
			// �������� �������� ������� �� ��������� ����� ���������� ���������
			view1.init((int)(Math.random()*100));
			// �������� ��������� view1.numbers
		try {
			view1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
			// �������� ��������� view2.numbers
		try {
		view2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
			// ������ ��������� ������� �� ���������, ������� ���������
		assertEquals(view1.getNumbers().size(), view2.getNumbers().size());
			// ������ ��� �������� ������ ���� �����.
			// ��� ����� ����� ���������� ����� equals
		assertTrue("containsAll()", view1.getNumbers().containsAll(view2.getNumbers()));
	}
}