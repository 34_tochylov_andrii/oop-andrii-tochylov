package test04;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ex01.Number;
import ex02.ViewResult;
import ex04.ChangeConsoleCommand;
import ex04.ChangeItemCommand;

/**
 * ������������ ������ ChangeItemCommand
 * 
 * @author Andrei
 * @version 4.0
 * @see ChangeItemCommand
 */
public class MainTest {
	/** �������� ������ {@linkplain ChangeItemCommand#execute()} */
	@Test
	public void testExecute() {
		ChangeItemCommand cmd = new ChangeItemCommand();
		cmd.setNumber(new Number());

		int offset;
		for (int ctr = 0; ctr < 1000; ctr++) {
			int x = ((int) (Math.random() * 100));
			cmd.getNumber().setNumbers(x, Integer.toBinaryString(x), Integer.toOctalString(x), Integer.toHexString(x));
			cmd.setOffset(offset = ((int) (Math.random() * 10)));
			cmd.execute();
			assertEquals(x * offset, cmd.getNumber().getValue());
			assertEquals(Integer.toBinaryString(x * offset), cmd.getNumber().getBinary());
			assertEquals(Integer.toOctalString(x * offset), cmd.getNumber().getOctal());
			assertEquals(Integer.toHexString(x * offset), cmd.getNumber().getHexadecimal());
		}
	}

	/** �������� ������ {@linkplain ChangeConsoleCommand} */
	@Test
	public void testChangeConsoleCommand() {
		ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult());
		cmd.getView().viewInit();
		cmd.execute();
		assertEquals("'c'hange", cmd.toString());
		assertEquals('c', cmd.getKey());
	}
}