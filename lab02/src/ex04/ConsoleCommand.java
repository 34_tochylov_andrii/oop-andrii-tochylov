package ex04;

/**
 * ��������� ���������� �������; ������ Command
 * 
 * @author Andrei
 * @version 1.0
 */
public interface ConsoleCommand extends Command {
	/**
	 * ������� ������� �������; ������ Command
	 * 
	 * @return ������ ������� �������
	 */
	public char getKey();
}