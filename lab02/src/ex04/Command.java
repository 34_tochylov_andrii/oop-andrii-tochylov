package ex04;

/**
 * ��������� ������� ��� ������; �������: Command, Worker Thread
 * 
 * @author Andrei
 * @version 1.0
 */
public interface Command {
	/** ���������� �������; �������: Command, Worker Thread */
	public void execute();
}