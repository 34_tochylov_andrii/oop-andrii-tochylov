package ex04;

import ex01.Number;

/**
 * ������� Change item; ������ Command
 * 
 * @author Andrei
 * @version 1.0
 */
public class ChangeItemCommand implements Command {
	/** �������������� ������; ������ Command */
	private Number number;
	/** �������� �������; ������ Command */
	private int offset;

	/**
	 * �������������� ���� {@linkplain ChangeItemCommand#item}
	 * 
	 * @param item �������� ��� {@linkplain ChangeItemCommand#item}
	 * @return ����� �������� {@linkplain ChangeItemCommand#item}
	 */
	public Number setNumber(Number number) {
		return this.number = number;
	}

	/**
	 * ���������� ���� {@linkplain ChangeItemCommand#item}
	 * 
	 * @return �������� {@linkplain ChangeItemCommand#item}
	 */
	public Number getNumber() {
		return number;
	}

	/**
	 * �������������� ���� {@linkplain ChangeItemCommand#offset}
	 * 
	 * @param offset �������� ��� {@linkplain ChangeItemCommand#offset}
	 * @return ����� �������� {@linkplain ChangeItemCommand#offset}
	 */
	public int setOffset(int offset) {
		return this.offset = offset;
	}

	/**
	 * ���������� ���� {@linkplain ChangeItemCommand#offset}
	 * 
	 * @return �������� {@linkplain ChangeItemCommand#offset}
	 */
	public double getOffset() {
		return offset;
	}

	@Override
	public void execute() {
		number.setBinary(Integer.toBinaryString(number.getValue() * offset));
		number.setOctal(Integer.toOctalString(number.getValue() * offset));
		number.setHexadecimal(Integer.toHexString(number.getValue() * offset));
		number.setValue(number.getValue() * offset);
	}
}