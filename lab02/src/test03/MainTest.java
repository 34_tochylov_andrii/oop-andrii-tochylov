package test03;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import ex01.Number;
import ex03.ViewTable;

/**
 * ��������� ������������ ������������� �������.
 * 
 * @author Andrei
 * @version 3.0
 */
public class MainTest {
	/** �������� �������� ���������������� ������ {@linkplain ViewTable} */
	@Test
	public void testCalc() {
		ViewTable tbl = new ViewTable(10, 5);
		assertEquals(10, tbl.getWidth());
		assertEquals(5, tbl.getNumbers().size());
		tbl.init(40, 10);
		Number number = new Number();
		int ctr = 0;
		number.setNumbers(0, "0", "0", "0");
		assertTrue("expected:<" + number + "> but was:<" + tbl.getNumbers().get(ctr) + ">",
				tbl.getNumbers().get(ctr).equals(number));
		ctr++;
		number.setNumbers(10, "1010", "12", "a");
		assertTrue("expected:<" + number + "> but was:<" + tbl.getNumbers().get(ctr) + ">",
				tbl.getNumbers().get(ctr).equals(number));
		ctr++;
		number.setNumbers(20, "10100", "24", "14");
		assertTrue("expected:<" + number + "> but was:<" + tbl.getNumbers().get(ctr) + ">",
				tbl.getNumbers().get(ctr).equals(number));
	}

	/** �������� ������������. ������������ �������������� ������. */
	@Test
	public void testRestore() {
		ViewTable tbl1 = new ViewTable(10, 1000);
		ViewTable tbl2 = new ViewTable();
// �������� �������� ������� �� ��������� ����� ���������� ���������
		tbl1.init(30, ((int) (Math.random() * 1000)));
// �������� ��������� tbl1.numbers
		try {
			tbl1.viewSave();
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		}
// �������� ��������� tbl2.numbers
		try {
			tbl2.viewRestore();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
// ������ ��������� ������� �� ���������, ������� ���������
		assertEquals(tbl1.getNumbers().size(), tbl2.getNumbers().size());
// ������ ��� �������� ������ ���� �����.
// ��� ����� ����� ���������� ����� equals
		assertTrue("containsAll()", tbl1.getNumbers().containsAll(tbl2.getNumbers()));
	}
}