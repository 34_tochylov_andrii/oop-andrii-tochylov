package ex03;

import ex02.View;
import ex02.ViewableResult;

/**
 * ConcreteCreator (������ �������������� Factory Method)<br>
 * ��������� �����, "�����������" �������
 * 
 * @author Andrei
 * @version 1.0
 * @see ViewableResult
 * @see ViewableTable#getView()
 */
public class ViewableTable extends ViewableResult {

	/** ������ ������������ ������ {@linkplain ViewTable} */
	@Override
	public View getView() {
		return new ViewTable();
	}
}