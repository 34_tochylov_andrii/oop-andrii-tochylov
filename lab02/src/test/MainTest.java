package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.Test;

import ex01.Calc;
import ex01.Number;

import org.junit.AfterClass;
import org.junit.BeforeClass;


/** ��������� ������������ ������������� �������.
* @author Andrei
* @version 1.0
*/
public class MainTest {
	
/** �������� ����� ��� ���������� �����*/
	private static final String FNAME = "testfile.bin";
/** ��������� ����� ��� ����������� */
	private static int val = (int)Math.random() * 10000;
	
/** �������� �������� ���������������� ������ {@linkplain Calc} */
	@Test
	public void testCalc() {
		Calc calc = new Calc();
		
		calc.init(0);
		assertEquals(calc.getResult().getBinary(), "0");
		calc.init(10);
		assertEquals(calc.getResult().getBinary(), "1010");
		calc.init(val);
		assertEquals(calc.getResult().getBinary(), Integer.toBinaryString(val));
		
		calc.init(0);
		assertEquals(calc.getResult().getOctal(), "0");
		calc.init(10);
		assertEquals(calc.getResult().getOctal(), "12");
		calc.init(val);
		assertEquals(calc.getResult().getOctal(), Integer.toOctalString(val));
		
		calc.init(0);
		assertEquals(calc.getResult().getHexadecimal(), "0");
		calc.init(10);
		assertEquals(calc.getResult().getHexadecimal(), "a");
		calc.init(val);
		assertEquals(calc.getResult().getHexadecimal(), Integer.toHexString(val));
	}
/** ������������ ������� �� ���������� ����� � �������� ��������� �����*/
	@BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
		Calc calc = new Calc();
        try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
			os.writeObject(calc.init(val));
			os.flush();
			os.close();
        } catch (Exception e) {
            fail("Exception thrown during test: " + e.toString());
        }
    }
/** �������� ��������� ����� ����� �����*/
	@AfterClass
    public static void tearDownAfterClass() throws Exception {
        // �������� �����
        new File(FNAME).delete();
    }
	
/** �������� ������������ �������������� ������. */
	@Test
	public void testRestore() {
		Number number = new Number();
		try {
			ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
			number = (Number)is.readObject();
			is.close();
			
			assertEquals(number.getValue(), val);
			assertEquals(number.getBinary(), Integer.toBinaryString(val));
			assertEquals(number.getOctal(), Integer.toOctalString(val));
			assertEquals(number.getHexadecimal(), Integer.toHexString(val));
			} catch (Exception e) {
				fail(e.getMessage());
			}
	}
	
	
}