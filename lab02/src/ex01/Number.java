package ex01;

import java.io.Serializable;

/** �������� ���������� ������� ��� ���������� � ����������� �����������.
* @author Andrei
* @version 1.0
*/
public class Number implements Serializable {
	
	/** ������������� ��������������� ��������� */
	private static final long serialVersionUID = 1L;
	
		/** ��������� �����*/	
	private int value;
		/** ������������ � �������� �������������*/
	private String binary;
		/** ������������ � ������������ �������������*/
	private String octal;
		/** ������������ � ����������������� �������������*/
	private String hexadecimal;

		
	/** ������������� �������� �����*/ 
	public Number() {
		value = 0;
		binary = "";
		octal = "";
		hexadecimal = "";
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
	/**
	 * @return the binary
	 */
	public String getBinary() {
		return binary;
	}
	/**
	 * @param binary the binary to set
	 */
	public void setBinary(String binary) {
		this.binary = binary;
	}
	/**
	 * @return the octal
	 */
	public String getOctal() {
		return octal;
	}
	/**
	 * @param octal the octal to set
	 */
	public void setOctal(String octal) {
		this.octal = octal;
	}
	/**
	 * @return the hexadecimal
	 */
	public String getHexadecimal() {
		return hexadecimal;
	}
	/**
	 * @param hexadecimal the hexadecimal to set
	 */
	public void setHexadecimal(String hexadecimal) {
		this.hexadecimal = hexadecimal;
	}
	/** ������������� �������� ����� ������ */
	public Number setNumbers(int val, String bin, String oct, String hex) {
		this.value = val;
		this.binary = bin;
		this.octal = oct;
		this.hexadecimal = hex;
	return this;
	}
	
	/** ������������ ��������� ���������� � ���� ������.<br>{@inheritDoc} */
	@Override
	public String toString() {
		return "Number: " +value+ "\nIn binary: " +binary+ "\nIn octal: " +octal+ "\nIn hexadecimal: " +hexadecimal;
	}
	
	
	/** ������������� ��������������� �����.<br>{@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((binary == null) ? 0 : binary.hashCode());
		result = prime * result + ((hexadecimal == null) ? 0 : hexadecimal.hashCode());
		result = prime * result + ((octal == null) ? 0 : octal.hashCode());
		result = prime * result + value;
		return result;
	}
	/** ������������� ��������������� �����.<br>{@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Number other = (Number) obj;
		if (binary == null) {
			if (other.binary != null)
				return false;
		} else if (!binary.equals(other.binary))
			return false;
		if (hexadecimal == null) {
			if (other.hexadecimal != null)
				return false;
		} else if (!hexadecimal.equals(other.hexadecimal))
			return false;
		if (octal == null) {
			if (other.octal != null)
				return false;
		} else if (!octal.equals(other.octal))
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}	
	
	
	
	
	