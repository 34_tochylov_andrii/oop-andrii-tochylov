package ex01;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/** �������� ���������� ������� ��� ���������� � ����������� �����������.
* @author Andrei
* @version 1.0
*/
public class Calc {
		/** ��� �����, ������������ ��� ������������. */
	private static final String FNAME = "Numbers.bin";
	
		/** ��������� ��������� ����������. ������ ������ {@linkplain Numbers} */
	private Number result;
	
		/** �������������� {@linkplain Calc#result} */
	public Calc() {
		result = new Number();
	}
	
		/** ���������� �������� {@linkplain Calc#result}
		* @param result - ����� �������� ������ �� ������ {@linkplain Number}
		*/
	public void setResult(Number result) {
		this.result = result;
	}
	
		/** �������� �������� {@linkplain Calc#result}
		* @return ������� �������� ������ �� ������ {@linkplain Number}
		*/
	
	public Number getResult() {
	return result;
	}
	
		/** ������������ � �������� �������������.
		* @param value - ��������� �����.
		* @return ��������� �����������.
		*/
	private String calcBin(int value) {
	return Integer.toBinaryString(value);
	}
	
		/** ������������ � ������������ �������������.
		* @param value - ��������� �����.
		* @return ��������� �����������.
		*/
	private String calcOctal(int value) {
	return Integer.toOctalString(value);
	}
	
		/** ������������ � ����������������� �������������.
		* @param value - ��������� �����.
			* @return ��������� �����������.
		*/
	private String calcHex(int value) {
	return Integer.toHexString(value);
	}
		/** ��������� �������� ������� � ���������
		* ��������� � ������� {@linkplain Calc#result}
		* @param val - ����� ��� �����������.
		*/
	public Number init(int val) {
		result.setValue(val);
		result.setBinary(calcBin(val));
		result.setOctal(calcOctal(val));
		result.setHexadecimal(calcHex(val));
	return result;
	}
		/** ������� ��������� �����������. */
	public void show() {
	System.out.println(result);
	}
		/** ��������� {@linkplain Calc#result} � ����� {@linkplain Calc#FNAME}
		* @throws IOException
		*/
	public void save() throws IOException {
	ObjectOutputStream os = new ObjectOutputStream(new
	FileOutputStream(FNAME));
	os.writeObject(result);
	os.flush();
	os.close();
	}
		/** ��������������� {@linkplain Calc#result} �� ����� {@linkplain Calc#FNAME}
		* @throws Exception
		*/
	public void restore() throws Exception {
	ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
	result = (Number)is.readObject();
	is.close();
}
}
